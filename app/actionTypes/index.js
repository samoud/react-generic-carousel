import pixabayTypes from './pixabay';

export default {
  ...pixabayTypes
};
