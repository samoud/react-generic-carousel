import { createSelector } from 'reselect';

const selectPixabayState = () => state => (state.pixabay);

const selectPictures = () => createSelector(
  selectPixabayState(),
  templateState => templateState.pictures
);

const selectFetchPicturesRequest = () => createSelector(
  selectPixabayState(),
  templateState => templateState.fetchPicturesRequest
);

export {
  selectPictures,
  selectFetchPicturesRequest
};
