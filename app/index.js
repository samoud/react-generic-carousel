import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { Provider } from 'react-redux';

import configureStore from 'store/configureStore';

// Containers
import App from 'containers/pages/App';

// CSS
import 'normalize.css/normalize.css';
import './styles/index.less';

const store = configureStore();

const Main = () => (
  <Provider store={ store }>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={ App } />
      </Switch>
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(<Main />, document.getElementById('app'));
