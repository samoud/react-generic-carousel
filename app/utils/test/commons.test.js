import { calculateCenterPosition } from '../commons';

describe('Utils - Commons - calculateCenterPosition', () => {
  test('should returns 0 if there is no children', () => {
    const position = calculateCenterPosition();

    expect(position).toEqual(0);
  });

  test('should calculate right position for one child', () => {
    window.innerWidth = 500;
    const items = {
      childNodes: [
        { offsetWidth: 100 }
      ]
    };

    const position = calculateCenterPosition(items);

    expect(position).toEqual(200);
  });

  test('should calculate right position for multiple children', () => {
    window.innerWidth = 500;
    const items = {
      childNodes: [
        { offsetWidth: 100 },
        { offsetWidth: 50 }
      ]
    };

    const firstPosition = calculateCenterPosition(items);
    const secondPosition = calculateCenterPosition(items, 1);

    expect(firstPosition).toEqual(200);
    expect(secondPosition).toEqual(125);
  });
});
