export function throttle(callback, wait, context = this) {
  let timeout = null;
  let callbackArgs = null;

  const later = () => {
    callback.apply(context, callbackArgs);
    timeout = null;
  };

  return (...args) => {
    if (!timeout) {
      callbackArgs = args;
      timeout = setTimeout(later, wait);
    }
  };
}

export function calculateCenterPosition(items, currentIndex = 0) {
  let totalWidthBeforeCurrent = 0;

  if (!items) return 0;

  const elements = items.childNodes;
  const currentWidth = elements[currentIndex].offsetWidth;

  for (let i = 0; i < currentIndex; i += 1) {
    totalWidthBeforeCurrent += elements[i].offsetWidth;
  }

  const position = (window.innerWidth / 2) - (totalWidthBeforeCurrent + (currentWidth / 2));

  return position;
}
