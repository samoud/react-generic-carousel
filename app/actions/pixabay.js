import actionTypes from 'actionTypes/pixabay';

export default {
  fetchPictures: () => ({ type: actionTypes.FETCH_PICTURES_REQUEST })
};
