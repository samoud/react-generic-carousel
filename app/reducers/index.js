import { combineReducers } from 'redux';

import pixabay from './pixabay';

const appReducer = combineReducers({
  pixabay
});


export default (state, action) => {
  if (action.type === 'RESET_STORE') {
    return appReducer(undefined, action);
  }

  return appReducer(state, action);
};
