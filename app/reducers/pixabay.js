import actionTypes from 'actionTypes/pixabay';

export const initialState = {
  pictures: [],
  fetchPicturesRequest: { loading: null, error: null }
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    /* GET_MY_TEMPLATES_REQUEST */
    case actionTypes.FETCH_PICTURES_REQUEST:
      return {
        ...state,
        fetchPicturesRequest: { loading: true, error: false }
      };

    case actionTypes.FETCH_PICTURES_SUCCESS:
      return {
        ...state,
        fetchPicturesRequest: { loading: false, error: false },
        pictures: action.pictures.slice(0, 6)
      };

    case actionTypes.FETCH_PICTURES_FAILURE:
      return {
        ...state,
        fetchPicturesRequest: { loading: false, error: action.message },
        pictures: []
      };

    /* DEFAULT */
    default:
      return state;
  }
}
