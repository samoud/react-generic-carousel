import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import actions from 'actions';
import { selectPictures, selectFetchPicturesRequest } from 'selectors/pixabay';

import Headline from 'components/Headline';
import Loader from 'components/Loader';

import Carousel from 'containers/fragments/Carousel';

import styles from './styles.less';

export class App extends Component {
  componentDidMount() {
    const { fetchPictures } = this.props;

    fetchPictures();
  }

  render() {
    const { pictures, fetchPicturesRequest } = this.props;

    const showLoader = fetchPicturesRequest.loading || !pictures.length;

    return (
      <div className={ styles.container }>
        <Headline>
          Carousel Test
        </Headline>

        { showLoader
          ? <Loader />
          : <Carousel pictures={ pictures } />
        }
      </div>
    );
  }
}

App.propTypes = {
  pictures: PropTypes.instanceOf(Array).isRequired,
  fetchPicturesRequest: PropTypes.instanceOf(Object).isRequired,
  fetchPictures: PropTypes.func.isRequired
};

App.defaultProps = {};

const mapStateToProps = createStructuredSelector({
  pictures: selectPictures(),
  fetchPicturesRequest: selectFetchPicturesRequest()
});

const mapDispatchToProps = dispatch => ({
  fetchPictures: () => dispatch(actions.fetchPictures())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
