import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';

import { App } from 'containers/pages/App';
import Headline from 'components/Headline';
import Loader from 'components/Loader';
import Carousel from 'containers/fragments/Carousel';

function renderComponent(props) {
  const renderer = new ShallowRenderer();
  renderer.render(<App { ...props } />);
  return renderer.getRenderOutput();
}

describe('Utils - Commons - calculateCenterPosition', () => {
  test('should display loading if there is no picture', () => {
    const component = renderComponent({
      pictures: [],
      fetchPictures: jest.fn(),
      fetchPicturesRequest: { loading: false }
    });

    expect(component.props.children).toEqual([
      <Headline>Carousel Test</Headline>,
      <Loader />
    ]);
  });

  test('should display loading if fetch request did not finish', () => {
    const component = renderComponent({
      pictures: ['test'],
      fetchPictures: jest.fn(),
      fetchPicturesRequest: { loading: true }
    });

    expect(component.props.children).toEqual([
      <Headline>Carousel Test</Headline>,
      <Loader />
    ]);
  });

  test('should display loading if there is pictures', () => {
    const component = renderComponent({
      pictures: ['test'],
      fetchPictures: jest.fn(),
      fetchPicturesRequest: { loading: false }
    });

    expect(component.props.children).toEqual([
      <Headline>Carousel Test</Headline>,
      <Carousel pictures={ ['test'] } />
    ]);
  });
});
