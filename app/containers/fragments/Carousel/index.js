import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { throttle, calculateCenterPosition } from 'utils/commons';

import CarouselItem from 'components/CarouselItem';
import CarouselNavigation from 'components/CarouselNavigation';

import styles from './styles.less';

class Carousel extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = { currentIndex: 0 };
  }

  componentDidMount() {
    this.setPosition();
    window.addEventListener('resize', throttle(this.updatePosition, 250));
  }

  onPrevClick = () => {
    const { currentIndex } = this.state;
    const prevIndex = currentIndex - 1;

    this.setPosition(prevIndex);
    this.setState({ currentIndex: prevIndex });
  }

  onNextClick = () => {
    const { currentIndex } = this.state;
    const nextIndex = currentIndex + 1;

    this.setPosition(nextIndex);
    this.setState({ currentIndex: nextIndex });
  }

  setPosition(currentIndex) {
    const position = calculateCenterPosition(this.items, currentIndex);
    this.setState({ position });
  }

  updatePosition = () => {
    const { currentIndex } = this.state;
    this.setPosition(currentIndex);
  }

  render() {
    const { className, pictures } = this.props;
    const { currentIndex, position } = this.state;

    const transformStyle = { transform: `translate3d(${position}px, 0, 0)` };
    const isPrevDisabled = currentIndex === 0;
    const isNextDisabled = currentIndex === pictures.length - 1;

    return (
      <div className={ classnames(styles.carousel, className) }>
        <div
          className={ styles.items }
          style={ transformStyle }
          ref={ (elem) => { this.items = elem; } }
        >
          { pictures.map(picture => <CarouselItem key={ picture.id } picture={ picture } />) }
        </div>

        <CarouselNavigation
          prevDisabled={ isPrevDisabled }
          onPrevClick={ this.onPrevClick }
          nextDisabled={ isNextDisabled }
          onNextClick={ this.onNextClick }
        />
      </div>
    );
  }
}

Carousel.propTypes = {
  className: PropTypes.string,
  pictures: PropTypes.instanceOf(Array).isRequired
};

Carousel.defaultProps = {
  className: null
};

export default Carousel;
