import { createStore, applyMiddleware, compose } from 'redux';

import reducers from 'reducers';
import createSagaMiddleware from 'redux-saga';
import sagas from 'sagas';

export default(initialState = {}) => {
  const middlewares = [];

  // Create the saga middleware
  const sagaMiddleware = createSagaMiddleware();
  middlewares.push(sagaMiddleware);

  // Create and export the store
  let createStoreWithMiddleware = applyMiddleware(...middlewares);

  // Create the logger
  if (process.env.NODE_ENV !== 'production') {
    // eslint-disable-next-line
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    // Create and export the store
    createStoreWithMiddleware = composeEnhancers(applyMiddleware(...middlewares));
  }

  const finalCreateStore = createStoreWithMiddleware(createStore);
  const store = finalCreateStore(reducers, initialState);

  // Start the sagas
  sagaMiddleware.run(sagas);

  return store;
};
