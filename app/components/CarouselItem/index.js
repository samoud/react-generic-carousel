import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.less';

function CarouselItem({ className, picture }) {
  const { user, webformatURL, webformatWidth } = picture;

  const imageDynamicStyle = {
    width: webformatWidth
  };

  return (
    <div className={ classnames(styles.item, className) } style={ imageDynamicStyle }>
      <img className={ styles.image } src={ webformatURL } alt={ user } />
      <span className={ styles.label }>
        { user }
      </span>
    </div>
  );
}

CarouselItem.propTypes = {
  className: PropTypes.string,
  picture: PropTypes.instanceOf(Object).isRequired
};

CarouselItem.defaultProps = {
  className: null
};

export default CarouselItem;
