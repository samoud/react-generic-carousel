import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.less';

function Headline({ className, children }) {
  return (
    <h1 className={ classnames(styles.headline, className) }>
      { children }
    </h1>
  );
}

Headline.propTypes = {
  className: PropTypes.string,
  children: PropTypes.string.isRequired
};

Headline.defaultProps = {
  className: null
};

export default Headline;
