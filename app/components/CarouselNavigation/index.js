import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.less';

function CarouselNavigation(props) {
  const {
    className,
    prevDisabled,
    nextDisabled,
    onPrevClick,
    onNextClick
  } = props;

  return (
    <div className={ classnames(styles.navigation, className) }>
      <button className={ classnames(styles.button, styles.previous) } disabled={ prevDisabled } type="button" onClick={ onPrevClick }>
        <img className={ styles.icon } src="assets/images/arrow.svg" alt="previous" />
        <span className={ styles.label }>
          Prev
        </span>
      </button>
      <button className={ classnames(styles.button, styles.next) } disabled={ nextDisabled } type="button" onClick={ onNextClick }>
        <img className={ styles.icon } src="assets/images/arrow.svg" alt="next" />
        <span className={ styles.label }>
          Next
        </span>
      </button>
    </div>
  );
}

CarouselNavigation.propTypes = {
  className: PropTypes.string,
  prevDisabled: PropTypes.bool.isRequired,
  nextDisabled: PropTypes.bool.isRequired,
  onPrevClick: PropTypes.func.isRequired,
  onNextClick: PropTypes.func.isRequired
};

CarouselNavigation.defaultProps = {
  className: null
};

export default CarouselNavigation;
