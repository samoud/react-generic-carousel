import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.less';

function Loader({ className }) {
  return (
    <div className={ classnames(styles.loader, className) }>
      Loading ...
    </div>
  );
}

Loader.propTypes = {
  className: PropTypes.string
};

Loader.defaultProps = {
  className: null
};

export default Loader;
