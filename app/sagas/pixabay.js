import { call, put } from 'redux-saga/effects';
import axios from 'axios';

import actionTypes from 'actionTypes/pixabay';

import config from 'utils/config';

export function fetchPicturesApi() {
  const { pixabay: { basepath, apikey } } = config;

  return axios.get(`${basepath}/?key=${apikey}&q=beautiful+landscape&image_type=photo`);
}

export function* fetchPictures() {
  try {
    const result = yield call(fetchPicturesApi);

    if (result.error) {
      yield put({ type: actionTypes.FETCH_PICTURES_FAILURE, message: result.error });
    } else {
      yield put({ type: actionTypes.FETCH_PICTURES_SUCCESS, pictures: result.data.hits });
    }
  } catch (e) {
    yield put({ type: actionTypes.FETCH_PICTURES_FAILURE, message: e.message });
  }
}
