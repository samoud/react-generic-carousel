import { call, put } from 'redux-saga/effects';
import axios from 'axios';

import actionTypes from 'actionTypes/pixabay';

import pixabayResponse from './pixabay';
import { fetchPictures, fetchPicturesApi } from '../pixabay';

describe('pixabay redux-saga testing', () => {
  test('should calls fetchPictures API', () => {
    const iterator = fetchPictures();

    expect(iterator.next().value).toEqual(call(fetchPicturesApi));
  });

  test('should dispatch FETCH_PICTURES_SUCCESS if the call succeeds', () => {
    const iterator = fetchPictures();

    const res = { data: { ...pixabayResponse } };
    axios.get = jest.fn(() => new Promise(resolve => resolve(res)));

    iterator.next();

    expect(iterator.next(res).value)
      .toEqual(
        put({ type: actionTypes.FETCH_PICTURES_SUCCESS, pictures: pixabayResponse.hits })
      );
  });

  test('should dispatch FETCH_PICTURES_FAILURE if the call fails', () => {
    const iterator = fetchPictures();

    const res = { error: 'error' };
    axios.get = jest.fn(() => new Promise(resolve => resolve(res)));

    iterator.next();

    expect(iterator.next(res).value)
      .toEqual(
        put({ type: actionTypes.FETCH_PICTURES_FAILURE, message: 'error' })
      );
  });

  test('should dispatch FETCH_PICTURES_FAILURE if the process fails', () => {
    const iterator = fetchPictures();
    const errorMessage = { message: 'error message' };

    iterator.next();

    expect(iterator.throw(errorMessage).value)
      .toEqual(
        put({ type: actionTypes.FETCH_PICTURES_FAILURE, ...errorMessage })
      );
  });
});
