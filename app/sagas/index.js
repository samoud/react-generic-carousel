import { all, takeEvery } from 'redux-saga/effects';
import actionTypes from 'actionTypes';

import { fetchPictures } from './pixabay';

export default function* rootSaga() {
  yield all([
    takeEvery(actionTypes.FETCH_PICTURES_REQUEST, fetchPictures)
  ]);
}
