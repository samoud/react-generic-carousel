const path = require('path');
const express = require('express');
const cors = require('cors');
const compression = require('compression');

const app = express();

// 3rd party middleware
app.use(cors());
app.use(compression());

app.use(express.static(path.join(__dirname, 'build')));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'build/index.html'));
});

const port = process.env.PORT || 8080;

app.listen(8080, () => {
  console.log(`--> Listening on port : ${port}`);
});
