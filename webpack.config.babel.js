import path from 'path';
import webpack from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';

const ENV = process.env.NODE_ENV || 'development';
const isProd = ENV === 'production';

module.exports = {
  mode: ENV,

  entry: {
    app: './app'
  },
  output: {
    path: path.resolve(__dirname, './build'),
    publicPath: '/',
    filename: '[name].[hash].js',
    chunkFilename: '[name].[hash].js'
  },

  resolve: {
    extensions: [
      '.jsx', '.js', '.json', '.less'
    ],
    modules: [
      'app',
      'node_modules'
    ]
  },

  module: {
    rules: [
      // JS
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [{ loader: 'babel-loader' }]
      },

      // LESS
      {
        test: /\.less$/,
        use: [
          {
            loader: 'style-loader'
          }, {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 2,
              sourceMap: true,
              localIdentName: '[local]___[hash:base64:5]'
            }
          }, {
            loader: 'less-loader'
          }
        ]
      },

      // CSS
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          }, {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 2,
              sourceMap: true,
              localIdentName: '[local]___[hash:base64:5]'
            }
          }
        ]
      },

      // SVG
      {
        test: /\.(svg)$/,
        use: [
          {
            loader: 'svg-url-loader'
          }
        ]
      }
    ]
  },

  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          chunks: 'initial',
          name: 'vendor',
          test: 'vendor',
          enforce: true
        }
      }
    },
    runtimeChunk: true,
    minimizer: [
      new UglifyJsPlugin({
        sourceMap: !isProd
      })
    ]
  },

  plugins: ([
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({ title: 'Generic Carousel', template: 'app/index.ejs' }),
    new CopyWebpackPlugin([{ from: 'app/assets', to: 'assets/' }], { copyUnmodified: true }),
    new webpack.NamedModulesPlugin()
  ]).concat(isProd ? [
    new ExtractTextPlugin({
      filename: 'style.[hash].css'
    })
  ] : []),

  stats: {
    colors: true
  },

  devtool: ENV === 'production' ? 'source-map' : 'inline-source-map',

  devServer: {
    port: process.env.PORT || 3000,
    host: 'localhost',
    publicPath: '/',
    contentBase: './app',
    historyApiFallback: true,
    compress: isProd,
    inline: !isProd,
    hot: !isProd
  }
};
